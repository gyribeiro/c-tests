MKDIR = mkdir -p
CC = gcc
# -g    add debugging info
# -Wall turn on compiler warnnigs
CFLAGS = -g -Wall

INCDIR = include
LIBDIR = lib
SRCDIR = src

INCLUDES = -I$(INCDIR)

LFLAGS = -L$(LIBDIR)

SRCS = $(shell ls src)

OBJS = $(patsubst %,$(LIBDIR)/%,$(SRCS:.c=.o))

LIBS = $(patsubst %,-l%,$(SRCS:.c=.o))

MAIN = gyrprog

all: $(MAIN)

$(MAIN): $(OBJS)
	$(CC) $(CFLAGS) $(INCLUDES) -o $(MAIN) $(MAIN).c $(OBJS)

#.c.o:
$(LIBDIR)/%.o: $(SRCDIR)/%.c
	$(MKDIR) $(LIBDIR)
	$(CC) $(CFLAGS) $(INCLUDES) -c $^ -o $@

.PHONY: clean

clean:
	$(RM) -v *.o  src/*.o *~ src/*~ core $(MAIN)
	$(RM) -vr lib
	$(RM) -vr gyrprog.dSYM
