#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "gyrlib.h"

void sort(int *array, int arraySize){
    int i, temp, lastSwapPos, n;

    printf("\nBubble Sort:\n");
    n = arraySize -1;
    while(n!=0){
        lastSwapPos = 0;
        for(i=0; i < n; i++){
            if(array[i] > array[i+1]){
                temp = array[i];
                array[i] = array[i+1];
                array[i+1] = temp;
                lastSwapPos = i;
            }
            printf("%d ", array[i]);
        }
        n = lastSwapPos;
        printf("\n");
    }
    printArray(array, arraySize);
    return;
}

void bubbleSort(){
    int arraySize = 10;
    int array[arraySize];

    createRandomArray(array, arraySize);
    sort(array, arraySize);
    return;
}
