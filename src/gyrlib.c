#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <gyrinclude.h>
#include "gyrstr.h"
#include "gyrbubble.h"
#include "gyrinsertion.h"

void gyrFunction(void) {
    printf("\n===================.\n");
    printf("This is GYR program:\n\n");
    //printStr();
    //bubbleSort();
    insertionSort();
    printf("\n===================.\n");
    return;
}

int sumDigits(int i){
    int lastDigit = i % 10;    // mod 10 gives de last digit of a number (base 10)
    int restOfDigits = i / 10; // divide by 10 (int) remove de last digit from a number (base 10)

    if(i == 0){
        return 0;
    }
    return lastDigit + sumDigits(restOfDigits);
}

void runSumDigits(){
    int num = 123;
    printf("%i = %i", num, sumDigits(num));
    return;
}

int factorialRecursive(int i){
    if(i == 0 || i == 1){
        return 1;
    }
    return (i * factorialRecursive(i - 1));
}

void runFactorialRecursive(){
    int num = 5;
    printf("%i! = %i", num, factorialRecursive(num));
    return;
}

int compareFraction(fraction *a, fraction *b){
    float resultA;
    float resultB;

    resultA = (float)a->num / (float)a->denom;
    resultB = (float)b->num / (float)b->denom;
    printf("\n%f : %f\n", resultA, resultB);

    if(resultA == resultB){
        return 0;
    }
    return 1;
}

int genericLinearSearch(void *array, size_t elemSize, int length, void *key, int (*cmpFunc)(void*,void*)){
    int i;

    for(i=0; i<length; i++){
        void *currentElem = array + (elemSize * i);
        if (cmpFunc(currentElem, key) == 0){
            return i;
        }
    }
    return -1;
}

void runGenericLinearSearch(){
    fraction arrayFract[5] = { {1,2}, {2,3}, {3,4}, {4,5}, {5,6} };
    fraction key = {9,12};

    printf("%i", genericLinearSearch(arrayFract, sizeof(fraction), 5, &key, compareFraction));
    return;
}

int binarySearch(int *array, int length, int key){
    int mid;
    int bot = 0;
    int top = length - 1;
    int loop = 0;

    while(bot <= top){
        mid = (top+bot)/2;
        if(array[mid] == key){
            //return key;
            return loop;
        } else if(array[mid] < key){
            bot = mid + 1;
        } else if(array[mid] > key){
            top = mid - 1;
        }
        loop++;
    }
    return -1;
}

int linearSearch(int *array, int length, int key){
    int i;

    for(i=0; i<length; i++){
        if (array[i] == key){
            return i;
        }
    }
    return -1;
}

void runSearch(){
    int array[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
    int key = 19;

    printf("%i\n", binarySearch(array, 20, key));
    printf("%i\n", linearSearch(array, 20, key));
    return;
}

void genericSwap(void *a, void *b, size_t size){
    void * temp = malloc(size);
    memcpy(temp, a, size);
    memcpy(a, b, size);
    memcpy(b, temp, size);
    free(temp);
    return;
}

void runGenericSwap(){
    fraction fract1 = {1, 2};
    fraction fract2 = {3, 4};

    printf("Fraction 1: %i/%i\n", fract1.num, fract1.denom);
    printf("Fraction 2: %i/%i\n", fract2.num, fract2.denom);

    genericSwap(&fract1, &fract2, sizeof(fraction));

    printf("Fraction 1: %i/%i\n", fract1.num, fract1.denom);
    printf("Fraction 2: %i/%i\n", fract2.num, fract2.denom);

    return;
}

int add(int a, int b){
    return a + b;
}

int mult(int a, int b){
    return a * b;
}

void printCalc(int (*myFunc)(int, int), int arg0, int arg1){
    printf("Result is %i\n", myFunc(arg0, arg1));
    return;
}

void funcAsVar(){
    int (*myFunc)(int, int) = add;

    printf("My result is %i\n", myFunc(3, 3));

    printCalc(add, 2, 5);
    printCalc(mult, 2, 5);
}

void sizeOfTest(){
    int i[] = {1, 2 ,3}; // stack memory
    int *pI = malloc(sizeof(int) * 3); //heap memory

    printf("Real sizeof: %lu \n", sizeof(int) * 3);
    printf("Stack sizeof: %lu \n", sizeof(i)); // correct
    printf("Heap sizeof: %lu \n", sizeof(pI)); // wrong: size of pointer
    printf("Heap sizeof: %lu \n", sizeof(*pI)); // wrong: size of int (first element)

    free(pI);
    return;
}

void printWeather(season s){
    if (s == summer) {
        printf("HOT \n");
    } else if (s == fall) {
        printf("FINE \n");
    } else if (s == winter) {
        printf("COOL \n");
    } else {
        printf("FLOWER \n");
    }
    return;
}

void passByValue(int i){
    i = 99;
    return;
}

void passByReference(int *i){
    *i = 77;
    return;
}

void arrayPointer(){
    int i;
    int tuna[5] = {1,2,3,4,5};

    printf("Element \t Address \t Value \n");
    for(i=0; i<5 ; i++){
        printf("tuna[%d] \t %p \t %d \n", i, &tuna[i], tuna[i]);
    }

    printf("\ntuna: %p \n", tuna);
    printf("*tuna: %d \n", *tuna);
    printf("*(tuna+3): %d \n", *(tuna+3));
    printf("tuna+3: %p \n", tuna+3);

    return;
}

void arrayString(){
    char foo[] = "String one";
    char * bar = "String two";

    printf("%s \n", foo);
    printf("%s \n", bar);
    bar = "String three";
    printf("%s \n", bar);
    printf("%c \n", bar[2]);

    return;
}

void allocation(){
    int i, numElem;  // stack memory
    int total = 0;   // stack memory
    float avg;       // stack memory
    int * arrayElem; // stack memory

    numElem = 5;
    arrayElem = (int *) malloc(numElem * sizeof(int)); // heap memory

    srand(time(NULL));
    for(i=0; i<numElem; i++){
        arrayElem[i] = (rand() % 100) +1;
    }

    for(i=0; i<numElem; i++){
        printf("%d: %d \n", i, arrayElem[i]);
        total += arrayElem[i];
    }

    printf("%d \n", total);

    avg = (float)total / (float)numElem;

    printf("%.2f \n", avg);

    free(arrayElem);

    return;
}

void myStruc(){
    struct user gus;

    gus.userID = 1;
    strcpy(gus.name, "Gustavo");
    gus.tax = 0.1;

    printf("ID: %d \n", gus.userID);
    printf("Name: %s \n", gus.name);
    printf("Tax: %.2f \n", gus.tax);

    return;
}

void printArray(int *array, int arraySize){
    int i;
    printf("\nSorted:\n");
    for(i=0; i < arraySize; i++){
        printf("%d ", array[i]);
    }
    printf("\n");
    return;
}

void createRandomArray(int *array, int arraySize){
    int i;
    srand(time(NULL));
    printf("\nOriginal:\n");
    for(i=0; i < arraySize; i++){
        array[i] = (rand() % 100) + 1;
        printf("%d ", array[i]);
    }
    return;
}
