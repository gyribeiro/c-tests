#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "gyrlist.h"

struct LIST * createRandomList(int listSize){
    int i;
    struct LIST *pHead = NULL;
    struct LIST *pCurrent = NULL;

    if(listSize < 1 ){
        return NULL;
    }

    printf("\nOriginal:\n");
    srand(time(NULL));

    pHead = malloc(sizeof(struct LIST));
    pHead->pNext = NULL;
    pHead->value = (rand() % 100) + 1;
    pCurrent = pHead;

    for(i=1; i < listSize; i++){
        pCurrent->pNext = malloc(sizeof(struct LIST));
        pCurrent->pNext->pNext = NULL;
        pCurrent->pNext->value = (rand() % 100) + 1;
        pCurrent = pCurrent->pNext;
    }

    return pHead;
}

void destroyList(struct LIST * pHead){
    struct LIST *pCurrent = pHead;

    while (pHead != NULL){
        pCurrent = pHead;
        pHead = pHead->pNext;
        free(pCurrent);
    }
    return;
}

void printList(struct LIST * pHead){
    struct LIST *pCurrent = pHead;

    printf("\n");
    while (pCurrent != NULL){
        printf("%i ", pCurrent->value);
        pCurrent = pCurrent->pNext;
    }
    printf("\n");
    return;
}
