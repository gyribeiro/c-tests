#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void myPrint(char *str){
    printf("%s\n", str);
    return;
}

void printStr(){
    int i = 0;
    char *strHeap = NULL;
    char *strRO = "01234";
    char *aux1 = "XXXXX";
    char *aux2 = "YYYYY";
    char strStack[] = "56789";

    // STACK
    myPrint(strStack);
    myPrint(strStack+1);
    printf("%c \n", strStack[1]);
    /* WRONG
     strStack = aux1;
     strStack = *aux1;
     strStack = "foobar";
     */
    strcpy(strStack, aux1);
    myPrint(strStack);
    for(i=0; i<5; i++){
        strStack[i] = aux2[i];
    }
    myPrint(strStack);


    // READ ONLY
    myPrint(strRO);
    myPrint(strRO+1);
    printf("%c \n", strRO[1]);
    /* WRONG
     strcpy(strRO, strStack);
     strcpy(strRO, aux1);
     for(i=0; i<5; i++){
         strRO[i] = aux1[i];
     }
     */
    strRO = aux1;
    myPrint(strRO);
    strRO = "foobar";
    myPrint(strRO);


    // HEAP
    strHeap = (char *)malloc(5 * sizeof(char));
    strcpy(strHeap, "43210");
    myPrint(strHeap);
    myPrint(strHeap+1);
    printf("%c \n", strHeap[1]);
    /* WRONG
     strHeap = aux1;
     strHeap = *aux1;
     strHeap = "foobar";
     */
    strcpy(strHeap, aux1);
    myPrint(strHeap);
    for(i=0; i<5; i++){
        strHeap[i] = aux2[i];
    }
    myPrint(strHeap);
    free(strHeap);

    return;
}
