#!/usr/bin/env sh

pushd $(dirname $0)/..
REPO_DIR=$(pwd)
DOCKER_DIR=/mnt
DISTRO=centos
DOCKER_IMAGE=gyr/${DISTRO}_c-tests:v1

docker run -v ${REPO_DIR}:${DOCKER_DIR} -t -i ${DOCKER_IMAGE} /bin/bash
docker ps -q -f status=exited | xargs docker rm

popd
