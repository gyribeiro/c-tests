#ifndef GYR_H_INCLUDED
#define GYR_H_INCLUDED

#define MYNUM 4

struct user{
    int userID;
    char name[40];
    float tax;
};

typedef enum {
    summer,
    fall,
    winter,
    spring
} season;

enum myEnum{
    one,
    two,
    three
};

typedef struct {
    int num;
    int denom;
} fraction;

void gyrFunction(void);

#endif // GYR_H_INCLUDED
