#ifndef GYR_H_LIST
#define GYR_H_LIST

struct LIST{
    struct LIST *pNext;
    int value;
};

void createRandomArray(int *array, int arraySize);

struct LIST * createRandomList(int listSize);

void destroyList(struct LIST * pHead);

void printList(struct LIST * pHead);

#endif // GYR_H_LIST
